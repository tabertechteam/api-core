package id.taber.main.domain.userRegistration.base;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;


@MappedSuperclass
public abstract class BaseUser<T extends BaseUser<T>> {

    @Id
    private String uuid;

    private String pin;


    private static  byte[] getSHA(String pin) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        return md.digest(pin.getBytes(StandardCharsets.UTF_8));
    }

    private static  String toHexString(byte[] hash){
        BigInteger number = new BigInteger(1,hash);
        StringBuilder hexstring = new StringBuilder(number.toString());

        while (hexstring.length()<32)
        {
            hexstring.insert(0,'0');
        }
        return  hexstring.toString();
    }

    public void generateUUID(){
        UUID uuid = UUID.randomUUID();
        this.uuid = uuid.toString();
    }

    public void setPin(String pin){
        try{
            this.pin = toHexString(getSHA(pin));
        }catch(NoSuchAlgorithmException e){
            System.out.println(e.getMessage());
        }
    }

    public String getUUID(){
        return uuid;
    }

    public String getPin(){
        return pin;
    }

}